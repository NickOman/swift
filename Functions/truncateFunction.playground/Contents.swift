

import UIKit

var str = "Hello, playground"


// Truncate Method


var companyName = "Logistics Shipping Company Test String"


func getShortenedString(str : String) -> String {
    
    var strTemp = str as NSString
    
    //Keep the first 30 characters.
    strTemp = strTemp.substring(to: 30) as NSString
    
    return strTemp as String
    
}



if companyName.characters.count > 29 {
    
    companyName = getShortenedString(str: companyName)
    
}

print(companyName.characters.count)