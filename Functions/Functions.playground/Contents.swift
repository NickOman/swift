// Functions

// Sequence of instructions that perform a specific task, packaged as a unit


// Example of non DRY programming

// Area Calculation for Room 1

let lengthNonDry = 10
let widthNonDry = 12

let areaNonDry = lengthNonDry * widthNonDry


// Area Calculation for Room 2

let secondLength = 14
let secondWidth = 8

let secondArea = secondWidth * secondLength


// Function Example

func areaWith(lengthOfRoom: Int, widthOfRoom: Int) -> Int {
    
    
    let areaOfRoom = lengthOfRoom * widthOfRoom

    return areaOfRoom
}

let myArea: Int = areaWith(lengthOfRoom: 22, widthOfRoom: 22)

myArea


// Argument Labels

func remove (havingValue value: String){
    print(value)
}

remove(havingValue: "A")


// Default Values


func carpetCostHaving(length: Int, width: Int,carpetColor color: String = "Tan") -> (price: Int, carpetColor: String) {

// Grey Carpet = 1.00 / sq ft
// Tan Carpet - 2.00 / sq ft
// Deep Blue carpet - 4.00 / sq ft
    
    let area = areaWith(lengthOfRoom: length, widthOfRoom: width)
    var price = 0

    switch color {
    case "Grey": price = area * 1
    case "Tan": price = area * 2
    case "Blue": price = area * 4
    default: "We dont sell that color"
    }
    return (price, color)
}

let result = carpetCostHaving(length: 10, width: 20)
result.price
result.carpetColor

// Scope
// Second array does not exist outside function

func arrayModifier(array: [Int]){

    var arrayOfInts = array
    arrayOfInts.append(5)
    
    var secondArray = arrayOfInts
}

var arrayOfInts = [1,2,3,4]

arrayModifier(array: arrayOfInts)



