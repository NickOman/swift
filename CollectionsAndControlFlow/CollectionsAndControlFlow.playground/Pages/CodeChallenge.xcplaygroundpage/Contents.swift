
// Code Challenge for Collections and Control Flow

var iceCream = [
    "CC": "Chocolate Chip",
    "AP": "Apple Pie",
    "PB": "Peanut Butter"
]

iceCream.updateValue("Rocky Road", forKey: "RR")

let applePie = iceCream["AP"]

iceCream.updateValue("Chocolate Chip Cookie Dough", forKey: "CC")

print(iceCream)


// Enter your code below
var results: [Int] = []

for multiplier in 1...10{

    results.append(multiplier * 6)
}

print(results)



let numbers = [2,8,1,16,4,3,9]
var sum = 0
var counter = 0

// Enter your code below

while counter < numbers.count {
    
    sum += numbers[counter]
    counter += 1
}

print(sum)





