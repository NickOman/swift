var todo: [String] = [
"Finish coding project",
"Go to Costco",
"Order book from Amazon",
"Shovel Driveway",
"Put away Legos"
]


//print(todo[0])

for task in todo {
    print(task)
}

// Ranges 

// Closed Range Operator

    for number in 1...10{
    
    print ("\(number) times 5 is equal to \(number * 5)")

}

let names = ["Anna", "Alex", "Brian", "Jack"]
let count = names.count
for i in 0..<count {
    print("Person \(i + 1) is called \(names[i])")
}
// Person 1 is called Anna
// Person 2 is called Alex
// Person 3 is called Brian
// Person 4 is called Jack




// While Loop

    // Example 1

    var x: Int = 0

    while x <= 20 {
    
    print(x)
    
    x += 1
}

    // Example 2

    var index = 0

    while index < todo.count {
    
        print (todo[index])
    
    index += 1
    
}

// Repeat While

    var counter = 1

while counter < 1 {
    print ("While Loop")
    counter += 1
}

repeat {
    print ("Repeat Loop")
    counter += 1
}while counter < 1





