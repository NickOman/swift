// Collections and Control Flow

// Arrays

var todo: [String] = ["Finish collections course", "Buy Groceries", "Respond to Emails"]

// Add new item to end of array using append method

todo.append("Pick up dry cleaning")

// Concatenating two arrays

[1,2,3] + [4]


// Concatenate array containging string literal to todo

todo = todo + ["Order book online"]

// Performing the same operation using the unary addition operator

todo += ["Order book online"]

// Immutable Arrays

let secondTaskList = ["Mow the lawn"]

// secondTaskList.append("Pay bills") error
// secondTaskList += ["someValue"] error

// Reading From Arrays

let firstTask = todo[0] // example of subscripting notation

let thirdTask = todo[2]

// Modifying Existing Values in an Array
// Mutating an array

todo[4] = "Brush teeth"

todo[0] = "Write some code"

// Insert Using Indexes

todo.insert("Watch netflix documentary", at: 2)

// Removing Items from Arrays
todo
todo.remove(at: 2)
todo

todo.count

// Dealing with Non Existent Data

todo[0]
todo[5]

// todo[6] Do not use an index value that is out of range


// Dictionaries


/*
 Airport Code (Key)      Airport Name (Value)
 
 LGA                     La Guardia
 LHR                     Heathrow
 CDG                     Charles de Gaulle
 HKG                     Hong Kong International
 DXB                     Dubai International
 */

var airportCodes: [String: String] = [   "LGA": "La Guardia",
                                         "LHR": "Heathrow",
                                         "CDG": "Charles de Gaulle",
                                         "HKG": "Hong Kong International",
                                         "DXB": "Dubai International"]

let currentWeather: [String: Double] = ["Temperature": 75.0]

// Reading from a dictionary

airportCodes["LGA"]
airportCodes["HKG"]

// Inserting Key Value Pairs

//Subscript Notation
airportCodes["SYD"] = "Sydney Airport"
airportCodes["LGA"] = "La Guardia International Airport"


// Update value method - Inserts into dictionary if does not exist - Updates if in dictionary
airportCodes.updateValue("Dublin Airport", forKey: "DUB")

// Removing Key Value Pairs

//Subscript Notation
airportCodes["DXB"] = "nil"

// Method Remove
airportCodes.removeValue(forKey: "CDG")

airportCodes

// Dealing with Non Existent Data

let newYorkAirport = airportCodes["LGA"]

type(of: newYorkAirport)

let orlandoAirport = airportCodes["MCO"]


airportCodes









