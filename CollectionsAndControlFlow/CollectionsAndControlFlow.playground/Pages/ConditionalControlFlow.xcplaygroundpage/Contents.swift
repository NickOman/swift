

// If Statements

var temperature = 9

if temperature < 12 {
    print("It's getting cold.  You will need a Jacket")
} else if temperature < 18{
    print("Wear a sweater")
}else {
    print("It's nice outside. Wear a t-shirt")
}

// Logical Operators

if temperature > 7 && temperature < 12 {
    
    print("Wear a scarf with your jacket")

}

var isRaining = false
var isSnowing = false

if isRaining || isSnowing {
    
    print("You will need boots")

}

if !isRaining {
    
    print("The sun is out")

}

if isRaining && isSnowing && temperature < 2 {
    
    print("You will need gloves")
}

if (isRaining || isSnowing) && temperature < 2 {
    
    print("You will need gloves")
    
}









