//: [Previous](@previous)

import Foundation

var str = "Hello, playground"

//: [Next](@next)



// Switch Statement

let airportCodes = ["LGA", "LHR", "CDG", "HKG", "DXB", "LGW", "JFK", "ORY"]

for airportCode in airportCodes {
    
    switch airportCode {
    case "LGA", "JFK": print("New York")
    case "LHR", "LGW": print("London")
    case "CDG", "ORY": print("Paris")
    case "HKG": print("Hong Kong")
    default: print("I dont know what city that airport is in")
    }
    
}



// Switch Statement with Ranges

import GameKit

let randomTemperature = GKRandomSource.sharedRandom().nextInt(upperBound: 160)

switch randomTemperature{
    
case 0..<32: print("You need a parka")
case 32...45: print("You will need a scarf with a jacket")
case 45..<70: print("You will need a light sweater")
case 70...100: print("Wear a t-shirt")
default: print("You are not on earth")
}

