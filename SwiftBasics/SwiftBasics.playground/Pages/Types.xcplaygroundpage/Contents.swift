
// String Concatenation

let country = "United States of America"
let state = "Ohio"
let city = "Cleveland"
let street = "East Street"
let buildingNumber = 222

let address = country + state + city
// let streetAddress = buildingNumber + street <- Does not compile



// Sting Interpolation

let interpolatedAddress = "\(country), \(state), \(city)"

let interpolatedStreetAddress = "\(buildingNumber) \(street)"

/*
 -----------------
 Integers
 -----------------
*/

let favoriteProgrammingLanguage = "Swift"
let year = 2014 // int


/*
 ------------------------
 Floating Point Numbers
 ------------------------
 */

var version = 3.1 // double


/*
 -----------------
 Boolean
 -----------------
 */

let isAwesome = true


/*
 -----------------
 Type Safety
 -----------------
 */

var someString = ""

// someString = 12.0

// Explicit Declaration

let bestPlayer: String = "Lebron James"
let averagePointsPerGame: Double = 30.1
let yearOfRetirement: Int = 2003
let hallOfFame: Bool = true











