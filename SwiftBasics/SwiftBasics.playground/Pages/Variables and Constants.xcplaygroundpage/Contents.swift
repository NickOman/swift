//: Playground - noun: a place where people can play

import UIKit


var str = "Hello, playground"
str = "Hello World"

var number = 10

number = 20

// Constants 

let language = "Swift"

// Naming Conventions

/* 
    Rule #1: Spaces not allowed
    let programmingLanguage = "Objective-C"
*/


/* 
    Rule #2: Use camel case
    let programminglanguage = Objective-C // Wrong!
 */

let programmingLanguage = "Objective-C"
let favoriteProgrammingLanguage = "Swift"

// Use Emoji create variables

var 🐭 = "mouse"

🐭
















