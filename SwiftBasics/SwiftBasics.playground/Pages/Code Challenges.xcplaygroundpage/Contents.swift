
// Concat and Interpolate Code Challenge

let name = "Nick"

let greeting = "Hi there, \(name)"

// Interpolation

let finalGreeting1 = "\(greeting). How are you?"

// Concatenation

let finalGreeting2 = greeting + " How are you?"



// Mixed Types Code Challenge

let title = "A Dance with Dragons"

var rating = 7.5

var isAvailable = false



// Swift Types Unit Code Challenge

let firstValue: Int = 11
let secondValue: Int = 12
let product = firstValue * secondValue
let output = "The product of \(firstValue) times \(secondValue) is \(product)"



