// Structs are value types

struct User {

    var fullName: String
    var email: String
    var age: Int


}

var someUser = User(fullName: "Nick Oman", email: "nickoman@me.com", age: 34)

var anotherUser = someUser

anotherUser.email
anotherUser.fullName

someUser.email = "Nick21227@gmail.com"

anotherUser.email



// Classes are reference types

class Person {

    var fullName: String
    var email: String
    var age: Int
    
    init(name: String, email: String, age: Int) {
        self.fullName = name
        self.email = email
        self.age = age
    }

}

var somePerson = Person(name: "Tim Cook", email: "tim.cook@apple.com", age: 54)

var anotherPerson = somePerson

somePerson.email = "tcook@apple.com"

anotherPerson.email






















