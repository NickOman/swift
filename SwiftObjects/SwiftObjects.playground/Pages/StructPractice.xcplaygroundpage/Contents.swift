// StructPractice

// User Struct

struct User {
    
    let name: String
    let email: String
    
}

let firstUser = User(name: "Nick", email: "nickoman@me.com")

firstUser.email
firstUser.name

// Book Struct

struct Book {
    let title: String
    let author: String
    let price: Double
}

let myBook = Book(title: "Catcher in the Rye", author: "J.D. Salinger", price: 5.00)



