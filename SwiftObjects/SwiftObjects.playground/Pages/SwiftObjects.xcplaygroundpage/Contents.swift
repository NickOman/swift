// Swift Objects

// Structs


struct Point {

    // Stored Properties
    
    let x: Int
    let y: Int
    
    
    init(x: Int, y: Int){
        
        self.x = x
        self.y = y
    
    
    }
    
    /// Returns the surrounding points in range of
    /// the current point
    func points (inRange range: Int = 1)-> [Point]{
        var results = [Point]()        
        let lowerBoundOfXRange = x - range
        let upperBoundOfXRange = x + range
        
        let lowerBoundOfYRange = y - range
        let upperBoundofYRange = y + range
        
        for xCoordinate in lowerBoundOfXRange...upperBoundOfXRange{
            
            for yCoordinate in lowerBoundOfYRange...upperBoundofYRange{
            
                let coordinatePoint = Point(x: xCoordinate, y: yCoordinate)
                results.append(coordinatePoint)
            }
        
        }
        
        
        return results
        
    }
    

}

//// Creating an instance of the struct
//
//let coordinatePoint = Point(x: 2, y: 2)
//
//coordinatePoint.points()
//
//let coordinatePoint2 = Point(x: 5, y: 7)
//
//coordinatePoint2.points()



// Classes 

// Enemies


class Enemy{
    
    var life: Int = 2
    let position: Point

    init(x: Int, y: Int) {
        self.position = Point(x: x, y: y)
        
    }
    
    func decreaseLife(by factor: Int){
        life -= factor
    }

}

// Super Enemy uses Inheritance

class SuperEnemy: Enemy{

    let isSuper: Bool = true
    
    override init(x: Int, y: Int) {
        super.init(x: x, y: y)
        self.life = 50
    }
    
}



// Towers

class Tower {
    
    let position: Point
    var range: Int = 1
    var strength: Int = 1
    
    init(x: Int, y: Int ){
        self.position = Point(x: x, y: y)
    }
    
    func fire(at enemy: Enemy){
        
        if isInRange(of: enemy){
        
            enemy.decreaseLife(by: strength)
            print("Direct Hit")
        }else{
            print("Out of Range")
        }
        
    }
    
    func isInRange(of enemy: Enemy) -> Bool {
        
        let availablePositions = position.points(inRange: range)
        
        for point in availablePositions{
            
            if point.x == enemy.position.x && point.y == enemy.position.y{
            
                return true
                
            }
        
        }
    
        return false
        
    }
}

class LaserTower: Tower {
    
    override init(x: Int, y: Int) {
        super.init(x: x, y: y)
        self.range = 100
        self.strength = 100
        
    }
    
    override func fire(at enemy: Enemy) {
        while enemy.life >= 0 {
            
            enemy.decreaseLife(by: strength)
        
        }
        print("Enemy Destroyed!")
    }

}





let tower = Tower(x: 0, y: 1)
let enemy = Enemy(x: 1, y: 1)

tower.fire(at: enemy)

let tower2 = Tower(x: 2, y: 2)
let emeny2 = Enemy(x: 6, y: 6)

tower2.fire(at: emeny2)

let superEnemy = SuperEnemy(x: 2, y: 3)

let laserTower = LaserTower(x: 4, y: 5)





