// Swift Objects


let x1: Int = 0
let y1: Int = 0

let coordinate1 : (x: Int, y: Int) = (0,0)

coordinate1.x

struct Point {

    // Stored Properties
    
    let x: Int
    let y: Int

}

// Creating an instance of the struct

let coordinatePoint = Point(x: 1, y: 2)


coordinatePoint.x

// User Struct

struct User {
    
    let name: String
    let email: String

}

let firstUser = User(name: "Nick", email: "nickoman@me.com")

firstUser.email
firstUser.name

// Book Struct

struct Book {
    let title: String
    let author: String
    let price: Double
}

let myBook = Book(title: "Catcher in the Rye", author: "J.D. Salinger", price: 5.00)




