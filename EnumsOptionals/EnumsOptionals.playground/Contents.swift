
// String Example

let week: [String] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]


func dayType(for day: String) -> String{

    
    switch day {
    case "Saturday", "Sunday":
        return "Weekend"
    case "Monday", "Tuesday", "Wednesday", "Thursday", "Friday":
        return "Weekday"
    default:
        return "This is not a valid day"
    }

}

func isNotificationMuted(on day: String) -> Bool {
    
    if day == "Weekday" {
        return true
    } else {
        return false
    }
}

let result = dayType(for: week[4])

let isMuted = isNotificationMuted(on: result)


// Enum Example

enum Day {
    
    case sunday
    case monday
    case tuesday
    case wednesday
    case thursday
    case friday
    case saturday

}

enum DayType{
    
    case weekday
    case weekend

}

func dayType(for day: Day) -> DayType{

    switch day {
        
    case Day.saturday, Day.sunday: return DayType.weekend
    //case Day.monday, Day.tuesday, Day.wednesday, Day.thursday, Day.friday: return DayType.weekday
    
        
    // Uses an enum as a return type
        
    default: return .weekday
    }
    
}

func isNotificationMuted(on type: DayType) -> Bool {
    
    switch type {
    case DayType.weekend: return true
    case DayType.weekday: return false
    
    }

}


let result2 = dayType(for: .monday)
let kind = isNotificationMuted(on: result2)



import UIKit

let type = dayType(for: .saturday)
let muted = isNotificationMuted(on: .weekend)























